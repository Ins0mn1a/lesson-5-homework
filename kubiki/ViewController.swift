//
//  ViewController.swift
//  kubiki
//
//  Created by Grisha Stetsenko on 29.08.2018.
//  Copyright © 2018 Grisha Stetsenko. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.

        buildPyramid()
    }
    
    let boxHeight = 32
    let boxWidth = 32
    var xPos = 100
    var yPos = 300
    var xMove = 42
    
    func drawBox() {
        let rect = CGRect.init(x: xPos, y: yPos, width: boxWidth, height: boxHeight)
        let box = UIView.init(frame: rect)
        box.backgroundColor = .red
        view.addSubview(box)
    }
    
    func buildLineFromBoxes(count: Int) {
        for i in 0..<count {
            drawBox()
            xPos += xMove
        }
        xPos = 100
    }
    
    func buildPyramid() {
        for i in 0..<3 {
            buildLineFromBoxes(count: 3 - i)
            yPos -= 42
            xPos += (i + 1) * xMove / 2
            
        }
        
    }
}

